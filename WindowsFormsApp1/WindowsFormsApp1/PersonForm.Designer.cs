﻿
namespace WindowsFormsApp1
{
    partial class PersonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonForm));
            this.PersonGrid = new DevExpress.XtraGrid.GridControl();
            this.PersonView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblPersonName = new DevExpress.XtraEditors.LabelControl();
            this.lblPersonSurname = new DevExpress.XtraEditors.LabelControl();
            this.lblAge = new DevExpress.XtraEditors.LabelControl();
            this.txtPersonName = new DevExpress.XtraEditors.TextEdit();
            this.txtAge = new DevExpress.XtraEditors.TextEdit();
            this.txtPersonSurname = new DevExpress.XtraEditors.TextEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PersonGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonSurname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // PersonGrid
            // 
            this.PersonGrid.Location = new System.Drawing.Point(248, 12);
            this.PersonGrid.MainView = this.PersonView;
            this.PersonGrid.Name = "PersonGrid";
            this.PersonGrid.Size = new System.Drawing.Size(540, 426);
            this.PersonGrid.TabIndex = 0;
            this.PersonGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.PersonView});
            this.PersonGrid.DoubleClick += new System.EventHandler(this.PersonGrid_DoubleClick);
            // 
            // PersonView
            // 
            this.PersonView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colPersonName,
            this.colPersonSurname,
            this.colAge});
            this.PersonView.GridControl = this.PersonGrid;
            this.PersonView.Name = "PersonView";
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "id";
            this.colId.Name = "colId";
            // 
            // colPersonName
            // 
            this.colPersonName.Caption = "Adı";
            this.colPersonName.FieldName = "personName";
            this.colPersonName.Name = "colPersonName";
            this.colPersonName.OptionsColumn.AllowEdit = false;
            this.colPersonName.Visible = true;
            this.colPersonName.VisibleIndex = 0;
            // 
            // colPersonSurname
            // 
            this.colPersonSurname.Caption = "Soyadı";
            this.colPersonSurname.FieldName = "personSurname";
            this.colPersonSurname.Name = "colPersonSurname";
            this.colPersonSurname.OptionsColumn.AllowEdit = false;
            this.colPersonSurname.Visible = true;
            this.colPersonSurname.VisibleIndex = 1;
            // 
            // colAge
            // 
            this.colAge.Caption = "Yaşı";
            this.colAge.FieldName = "age";
            this.colAge.Name = "colAge";
            this.colAge.OptionsColumn.AllowEdit = false;
            this.colAge.Visible = true;
            this.colAge.VisibleIndex = 2;
            // 
            // lblPersonName
            // 
            this.lblPersonName.Location = new System.Drawing.Point(12, 12);
            this.lblPersonName.Name = "lblPersonName";
            this.lblPersonName.Size = new System.Drawing.Size(13, 13);
            this.lblPersonName.TabIndex = 1;
            this.lblPersonName.Text = "Ad";
            // 
            // lblPersonSurname
            // 
            this.lblPersonSurname.Location = new System.Drawing.Point(12, 38);
            this.lblPersonSurname.Name = "lblPersonSurname";
            this.lblPersonSurname.Size = new System.Drawing.Size(30, 13);
            this.lblPersonSurname.TabIndex = 2;
            this.lblPersonSurname.Text = "Soyad";
            // 
            // lblAge
            // 
            this.lblAge.Location = new System.Drawing.Point(12, 64);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(17, 13);
            this.lblAge.TabIndex = 3;
            this.lblAge.Text = "Yaş";
            // 
            // txtPersonName
            // 
            this.txtPersonName.Location = new System.Drawing.Point(83, 9);
            this.txtPersonName.Name = "txtPersonName";
            this.txtPersonName.Size = new System.Drawing.Size(131, 20);
            this.txtPersonName.TabIndex = 4;
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(83, 61);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(131, 20);
            this.txtAge.TabIndex = 5;
            // 
            // txtPersonSurname
            // 
            this.txtPersonSurname.Location = new System.Drawing.Point(83, 35);
            this.txtPersonSurname.Name = "txtPersonSurname";
            this.txtPersonSurname.Size = new System.Drawing.Size(131, 20);
            this.txtPersonSurname.TabIndex = 6;
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(127, 145);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 38);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Əlavə et";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.ImageOptions.Image")));
            this.btnNew.Location = new System.Drawing.Point(12, 145);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(100, 38);
            this.btnNew.TabIndex = 9;
            this.btnNew.Text = "Yeni";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // PersonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPersonSurname);
            this.Controls.Add(this.txtAge);
            this.Controls.Add(this.txtPersonName);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.lblPersonSurname);
            this.Controls.Add(this.lblPersonName);
            this.Controls.Add(this.PersonGrid);
            this.Name = "PersonForm";
            this.Text = "PersonForm";
            this.Load += new System.EventHandler(this.PersonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PersonGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPersonSurname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl PersonGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView PersonView;
        private DevExpress.XtraEditors.LabelControl lblPersonName;
        private DevExpress.XtraEditors.LabelControl lblPersonSurname;
        private DevExpress.XtraEditors.LabelControl lblAge;
        private DevExpress.XtraEditors.TextEdit txtPersonName;
        private DevExpress.XtraEditors.TextEdit txtAge;
        private DevExpress.XtraEditors.TextEdit txtPersonSurname;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colAge;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
    }
}

