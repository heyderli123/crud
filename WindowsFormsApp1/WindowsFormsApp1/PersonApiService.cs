﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class PersonApiService
    {
        public async Task<List<Person>> GetAll()
        {
            using (var client = new HttpClient())
            {
                var data = await client.GetStringAsync("http://161.97.144.100:9990/ecare/api/person/all");

                var people = JsonConvert.DeserializeObject<List<Person>>(data);

                return people;
            }
        }

        public async Task<Person> FindById(int id)
        {
            using (var client = new HttpClient())
            {
                var data = await client.GetStringAsync($"http://161.97.144.100:9990/ecare/api/person/{id}");

                var person = JsonConvert.DeserializeObject<Person>(data);

                return person;
            }
        }

        public async Task Insert(Person person)
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(person);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PostAsync("http://161.97.144.100:9990/ecare/api/person/save", data);
            }
        }

        public async Task Update(Person person)
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(person);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PutAsync($"http://161.97.144.100:9990/ecare/api/person/update/{person.id}", data);
            }
        }
    }
}
