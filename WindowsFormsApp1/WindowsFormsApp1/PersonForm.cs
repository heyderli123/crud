﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class PersonForm : Form
    {
        private readonly PersonApiService _service;

        public PersonForm()
        {
            InitializeComponent();

            _service = new PersonApiService();
        }

        public int Id { get; set; }

        public string PersonName
        {
            get => txtPersonName.Text.Trim();
            set => txtPersonName.Text = value;
        }

        public string PersonSurname
        {
            get => txtPersonSurname.Text.Trim();
            set => txtPersonSurname.Text = value;
        }

        public byte Age
        {
            get => Convert.ToByte(txtAge.Text.Trim());
            set => txtAge.Text = value.ToString();
        }

        private bool ValidateUserInputs()
        {
            if (string.IsNullOrEmpty(PersonName))
            {
                txtPersonName.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(PersonSurname))
            {
                txtPersonSurname.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtAge.Text))
            {
                txtAge.Focus();
                return false;
            }

            return true;
        }

        private void FillView(Person person)
        {
            if (person != null)
            {
                Id = person.id;
                PersonName = person.personName;
                PersonSurname = person.personSurname;
                Age = person.age ?? 18;
            }
        }

        private Person GetFromView()
        {
            var person = new Person
            {
                id = Id,
                personName = PersonName,
                personSurname = PersonSurname,
                age = Age
            };

            return person;
        }

        private async void PersonGrid_DoubleClick(object sender, System.EventArgs e)
        {
            var selectedRow = PersonView.GetRow(PersonView.GetSelectedRows()[0]) as Person;

            if (selectedRow == null)
            {
                MessageBox.Show("Sətir seçilməyib!");
                return;
            }

            var person = await _service.FindById(selectedRow.id);

            FillView(person);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FillView(new Person());
        }

        private async void PersonForm_Load(object sender, EventArgs e)
        {
            var people = await _service.GetAll();

            PersonGrid.DataSource = people;

            FillView(new Person());
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            var person = GetFromView();

            if (person.id == 0) await _service.Insert(person);
            else await _service.Update(person);

            var people = await _service.GetAll();

            PersonGrid.DataSource = people;
        }
    }
}
