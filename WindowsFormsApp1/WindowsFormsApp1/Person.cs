﻿namespace WindowsFormsApp1
{
    public class Person
    {
        public Person()
        {
            age = 18;
        }

        public int id { get; set; }
        public string personName { get; set; }
        public string personSurname { get; set; }
        public byte? age { get; set; }
    }
}
